package pl.gumtree.ads

import org.scalatest.{Matchers, FlatSpec}
import org.scalatest.mock.MockitoSugar

class TextPolygonBuilderSpec extends FlatSpec with Matchers with MockitoSugar {

  "TextPolygonBuilder" should "parse empty string" in {
    def result = new TextPolygonBuilder("").build()
    result.getNumPoints should be (0)
  }

  it should "parse list of tipples" in {
    val triples = """
      |7.294922,61.391459,0.0
      |5.833740,60.705448,0.0
      |6.536865,60.321509,0.0
      |7.976074,60.919755,0.0
      |7.822266,61.238531,0.0
      |7.481689,61.386198,0.0
    """.stripMargin

    def result = new TextPolygonBuilder(triples).build()
    result.getNumPoints should be > 0
  }

  it should "parse list of tipples with whitespace on right and left" in {
    val triples = """
                       7.294922,61.391459,0.0
                       5.833740,60.705448,0.0
                       6.536865,60.321509,0.0
                       7.976074,60.919755,0.0
                       7.822266,61.238531,0.0
                       7.481689,61.386198,0.0
                  """

    def result = new TextPolygonBuilder(triples).build()
    result.getNumPoints should be > 0
  }
}
