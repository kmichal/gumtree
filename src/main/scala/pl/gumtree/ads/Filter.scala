package pl.gumtree.ads

trait Filter {
  
  def filter(apartments: List[Apartment]): List[Apartment]
}