package pl.gumtree.ads

import com.vividsolutions.jts.geom._

class PolygonsFilter(polygons: List[Polygon]) extends Filter {

  def contains(lat: Double, lon: Double): Boolean = {
    val latLon = LatLon(lat, lon)
    contains(latLon)
  }

  def contains(latLon: LatLon): Boolean = {
    val point = GeometryFactory.point(latLon)
    contains(point)
  }

  def contains(point: Point): Boolean = {
    for (polygon <- polygons) {
      if (polygon.contains(point)) {
        return true
      }
    }

    return false
  }

  def filter(apartments: List[Apartment]): List[Apartment] = {
    val apartmentsInArea = apartments filter { apartment => 
      apartment.latLon match {
        case Some(apartmentLatLon) => contains(apartmentLatLon)
        case None => false
      }
    }

    return apartmentsInArea
  }
}