package pl.gumtree.ads

object GumTreeAdsUrlBuilder {
  
  def apply(): GumTreeAdsUrlBuilder = new GumTreeAdsUrlBuilder()
}

class GumTreeAdsUrlBuilder {

  private var page: Option[Int] = None

  private var connection: Option[String] = None

  def page(page: Int): GumTreeAdsUrlBuilder = {
    this.page = Some(page)
    return this
  }

  def connection(connection: String): GumTreeAdsUrlBuilder = {
    this.connection = Some(connection)
    return this
  }

  def build(): String = connection.get + s"&Page=${ page.get }"
}