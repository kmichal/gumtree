package pl.gumtree.ads

case class LatLon(lat: Double, lon: Double)