package pl.gumtree.ads.gui

import scala.language.implicitConversions

import pl.gumtree.ads.Apartment
import pl.gumtree.ads.LatLon
import pl.gumtree.ads.GumTreeConfiguration
import pl.gumtree.ads.geocoder.AddressGeocoderCache

import javax.swing._
import java.awt.Frame
import java.awt.Color
import java.awt.BorderLayout
import java.awt.event.ActionListener
import java.awt.event.ActionEvent

import com.typesafe.scalalogging.slf4j.Logging

import org.openstreetmap.gui.jmapviewer._
import org.openstreetmap.gui.jmapviewer.interfaces._

import com.vividsolutions.jts.geom.Polygon

object ApartmentMarkerModel {
  implicit def ApartmentMarkerModelToApartment(apartmentMarkerModel: ApartmentMarkerModel): Apartment = apartmentMarkerModel.apartment
  implicit def ApartmentToApartmentMarkerModel(apartment: ApartmentMarkerModel): ApartmentMarkerModel = ApartmentMarkerModel(apartment, Color.YELLOW)
}

case class ApartmentMarkerModel(apartment: Apartment, color: Color)

class AdsMap(polygons: List[Polygon]) extends JFrame with Logging {

  private lazy val map = createAndCenterMap()

  private var searchMarker: Option[MapMarker] = None

  def initGUI() {
    initWindow()
    addPolygons()
  }

  private def createAndCenterMap(): JMapViewer = {
    val map = new JMapViewer()
    val mapcenter = GumTreeConfiguration.mapcenter
    map.setDisplayPositionByLatLon(mapcenter.lat, mapcenter.lon, 12)
    return map
  }

  private def initWindow() {
    setTitle("GumTree 0.1a")
    setSize(800, 600)
    setExtendedState(Frame.MAXIMIZED_BOTH)
    setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE)
    setLayout(new BorderLayout())

    addMapComposits()
  }

  private def addMapComposits() {
    val searchPanel = createSearchPanel()
    add(map, BorderLayout.CENTER)
    add(searchPanel, BorderLayout.PAGE_END)
  }

  private def createSearchPanel(): JPanel = {
    val searchPanel = new JPanel(new BorderLayout())
    val searchButton = new JButton("Search")
    val searchTextField = new JTextField()
    
    searchPanel.add(searchButton, BorderLayout.LINE_START)
    searchPanel.add(searchTextField, BorderLayout.CENTER)
    addSearchAction(searchButton, searchTextField)
    
    return searchPanel
  }

  private def addSearchAction(searchButton: JButton, searchTextField: JTextField) {
    val searchListener = createSearchListener(searchTextField)
    searchButton.addActionListener(searchListener)
    searchTextField.addActionListener(searchListener)
  }

  private def createSearchListener(searchTextField: JTextField): ActionListener = {
    return new ActionListener() {
      def actionPerformed(event: ActionEvent) {
        removeOldSearchMarker()

        val searchText = searchTextField.getText.trim
        val latLon = AddressGeocoderCache(searchText)
        latLon match {
          case Some(latLon) => searchFound(latLon)
          case None => logger.debug(s"cannot decode $searchText")
        }
      }
    }
  }

  private def removeOldSearchMarker() {
    searchMarker match {
      case Some(marker) => map.removeMapMarker(marker)
      case None =>
    }
  }

  private def searchFound(latLon: LatLon) {
    centerMap(latLon)
    val emptyAddress = ""
    val marker = addMarker(emptyAddress, latLon, Color.GREEN)
    searchMarker = Some(marker)
  }

  private def centerMap(latLon: LatLon) {
    val zoom = 16
    map.setDisplayPositionByLatLon(latLon.lat, latLon.lon, zoom)
  }

  def addApartments(apartments: List[ApartmentMarkerModel]) {
    for (apartment <- apartments) {
      addApartment(apartment)
    }
  }

  private def addApartment(apartment: ApartmentMarkerModel) {
    val label = s"${ apartment.address.orNull }, ${ apartment.price.orNull }"

    apartment.latLon match {
      case Some(apartmentLatLen) => addMarker(label, apartmentLatLen, apartment.color)
      case None =>
    }
  }

  private def addMarker(label: String, latLon: LatLon, color: Color): MapMarkerDot = {
    val coordinate = new Coordinate(latLon.lat, latLon.lon)
    val marker = new MapMarkerDot(label, coordinate)
    marker.setBackColor(color)
    map.addMapMarker(marker)

    return marker
  }

  private def addPolygons() {
    for (polygon <- polygons) {
      addPolygon(polygon)
    }
  }

  private def addPolygon(polygon: Polygon) {
    val coordinates = polygon.getCoordinates map { coordinate => new Coordinate(coordinate.x, coordinate.y) }
    val mapPolygon = new MapPolygonImpl(coordinates : _*)
    map.addMapPolygon(mapPolygon)
  }
}