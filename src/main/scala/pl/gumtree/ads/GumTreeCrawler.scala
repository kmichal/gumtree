package pl.gumtree.ads

import pl.gumtree.ads.types._

import com.typesafe.scalalogging.slf4j.Logging

import scala.collection.mutable._

class GumTreeCrawler(starPage: Int, endPage: Int, connection: String) extends Logging {

  private lazy val configuration = createConfig()

  def getApartments(): List[Apartment] = {
    val allApartments = new ListBuffer[Apartment]()

    for (page <- endPage to starPage by -1) {
      logger.debug(s"downloading ${ endPage - page + 1 }/${ endPage - starPage + 1 }")
      val pageApartments = tryGetApartments(page)
      allApartments ++= pageApartments
    }

    return allApartments.toList
  }

  private def tryGetApartments(page: Int): List[Apartment] = {
    try {
      return getApartments(page)
    } catch {
      case e: Throwable => {
        logger.error(s"$e")
        Thread.sleep(1000)
        return tryGetApartments(page)
      }
    }
  }

  private def getApartments(page: Int): List[Apartment] = {
    val pageUrl = configuration.page(page)
    val adsUrls = new GumTreeAdsPage(pageUrl).getPageUrls().reverse

    val apartments = new ListBuffer[Apartment]()
    for (adUrl <- adsUrls) {
      try {
        apartments += new GumTreeAd(adUrl).getApartment()
      } catch {
        case e: Throwable => logger.error(s"$e $adUrl")
      }
    }

    return apartments.toList
  }

  private def createConfig(): GumTreeAdsUrlBuilder = {
    GumTreeAdsUrlBuilder().connection(connection)
  }
}