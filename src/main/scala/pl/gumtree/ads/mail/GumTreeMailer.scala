package pl.gumtree.ads.mail

import pl.gumtree.ads.Apartment
import pl.gumtree.ads.GumTreeConfiguration

import java.util.Date
import java.text.SimpleDateFormat

import scala.collection.mutable.StringBuilder

import org.codemonkey.simplejavamail._
import javax.mail.Message.RecipientType

class GumTreeMailer {

  private val formater = new SimpleDateFormat("yyyy-MM-dd HH:mm")

  private val conf = GumTreeConfiguration.mailer

  val mailer = new Mailer(conf.smtp, conf.port, conf.user, conf.password, conf.authentication)

  def send(apartments: List[Apartment]) {
    val email = createtMail(apartments)
    mailer.sendMail(email)
  }

  private def createtMail(apartments: List[Apartment]): Email = {
    val email = new Email()
    val htmlBody = createHtmlBody(apartments)

    email.setSubject(s"[Gumtree] ${ formater.format(new Date()) } Ads count: ${ apartments.size }")
    email.setFromAddress(conf.sender, conf.sender)
    for (recipient <- conf.recipients) {
      email.addRecipient(recipient, recipient, RecipientType.TO)
    }
    email.setTextHTML(htmlBody)

    return email
  }

  private def createHtmlBody(apartments: List[Apartment]): String = {
    val sb = new StringBuilder()
    
    for (apartment <- apartments) {
      sb ++= apartment.toString
    }

    return sb.toString
  }
}