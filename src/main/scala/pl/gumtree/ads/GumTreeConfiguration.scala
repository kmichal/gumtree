package pl.gumtree.ads

import scala.xml.{Elem, XML, Node, NodeSeq}
import scala.language.postfixOps
import scala.collection.mutable._

import com.vividsolutions.jts.geom._ 
import org.codemonkey.simplejavamail.TransportStrategy
import java.io.File

case class MailerConfiguration(smtp: String, user: String, password: String, port: Int, sender: String, recipients: List[String], authentication: TransportStrategy)

object MailerConfiguration {
  
  def apply(mailer: NodeSeq): MailerConfiguration = {

    val user = mailer \ "user" text
    val password = mailer \ "password" text
    val sender = mailer \ "sender" text
    val port = (mailer \ "port" text).toInt
    val authentication = mailer \ "authentication" text match {
      case "ssl" => TransportStrategy.SMTP_SSL
      case "tls" => TransportStrategy.SMTP_TLS
      case _ => TransportStrategy.SMTP_PLAIN
    }
    val smtp = mailer \ "smtp" text
    val recipients = getRecipients(mailer.head \ "recipients")

    return MailerConfiguration(smtp, user, password, port, sender, recipients, authentication)
  }

  private def getRecipients(xmlRecipientsNode: NodeSeq): List[String] = {
    val xmlRecipients = xmlRecipientsNode.head \\ "recipient"

    val recipients = new ListBuffer[String]
    for (xmlRecipient <- xmlRecipients) {
      recipients += xmlRecipient.text
    }

    return recipients.toList
  }
}

case class CrawlerConfiguration(startPage: Int, endPage: Int, connection: String)

object CrawlerConfiguration {
  
  def apply(crawler: NodeSeq): CrawlerConfiguration = {
    val startPage = (crawler \ "startpage" text).toInt
    val endPage = (crawler \ "endpage" text).toInt
    val connection = crawler \ "connection" text

    return CrawlerConfiguration(startPage, endPage, connection)
  }
}

case class PolygonsConfiguration(polygons: List[Polygon])

trait PolygonBuilder {
  def build(): Polygon
}

class XMLPolygonBuilder(xmlPolygon: Node) extends PolygonBuilder {

  override def build(): Polygon = {
    val xmlPoints = xmlPolygon \\ "point"

    val points = new ListBuffer[LatLon]
    for (xmlPoint <- xmlPoints) {
      points += createLatLonFromXml(xmlPoint)
    }

    return GeometryFactory.polygon(points.toList)
  }

  private def createLatLonFromXml(xmlPoint: Node): LatLon = {
    val lat = (xmlPoint \ "@lat" text).toDouble
    val lon = (xmlPoint \ "@lon" text).toDouble

    return LatLon(lat, lon)
  }
}

class TextPolygonBuilder(text: String) extends PolygonBuilder {

  type Triple = (Double, Double, Double)

  override def build(): Polygon = {
    val lines = splitToLines(text)
    val trimmedLines = trimLines(lines)
    val notEmpty = filterEmptyLines(trimmedLines)
    val triples = splitToTriples(notEmpty)
    val latLonList = convertTriplesToLatLons(triples)
    val polygon = GeometryFactory.polygon(latLonList)
    return polygon
  }

  private def splitToLines(textToSplit: String): List[String] = {
    textToSplit.split('\n').toList
  }

  private def trimLines(notTrimmedLines: List[String]): List[String] = {
    notTrimmedLines.map(_.trim)
  }

  private def filterEmptyLines(lines: List[String]): List[String] = {
    lines.filterNot(_.isEmpty)
  }

  private def splitToTriples(lines: List[String]): List[Triple] = {
    val triples = lines map splitToTriple
    return triples
  }

  private def splitToTriple(line: String): Triple = {
    line.split(',').toList match {
      case a +: b +: c +: tail => (a.trim.toDouble, b.trim.toDouble, c.trim.toDouble)
      case _ => throw new IllegalArgumentException()
    }
  }

  private def convertTriplesToLatLons(triples: List[Triple]): List[LatLon] = {
    val latLons = triples.map { case (lon, lat, _) => LatLon(lat, lon) }
    return latLons
  }
}

object PolygonBuilderProvider {
  def apply(xmlPolygon: Node): PolygonBuilder = {
    val numberOfPoints = (xmlPolygon \\ "point").size
    val builder = numberOfPoints match {
      case 0 => new TextPolygonBuilder(xmlPolygon.text)
      case _ => new XMLPolygonBuilder(xmlPolygon)
    }
    return builder
  }
}

object PolygonsConfiguration {

  def apply(xmlPolygons: NodeSeq): PolygonsConfiguration = {
    val polygons = new ListBuffer[Polygon]
    for (xmlPolygon <- xmlPolygons) {
      val polygonBuilder = PolygonBuilderProvider(xmlPolygon)
      polygons += polygonBuilder.build()
    }
    return PolygonsConfiguration(polygons.toList)
  }
}

object MapConfiguration {

  def apply(xmlMapCenter: NodeSeq): LatLon = {
    val lat = (xmlMapCenter \ "@lat" text).toDouble
    val lon = (xmlMapCenter \ "@lon" text).toDouble

    return LatLon(lat, lon)
  }
}

object IgnoredAdsConfiguration {

  def apply(ignoredAdsXML: NodeSeq): List[String] = {
    val ignoredAds = ignoredAdsXML \\ "ignoredad"

    val ignoredAdsText = new ListBuffer[String]
    for (ignoredAd <- ignoredAds) {
      ignoredAdsText += ignoredAd.text
    }

    return ignoredAdsText.toList
  }
}

object XMLProvider {
  def getXML(): Elem = {
    val gumtreeFileInCurrentDir = new File("gumtree.xml")
    if (gumtreeFileInCurrentDir.exists()) {
      XML.loadFile(gumtreeFileInCurrentDir)
    } else {
      XML.load(getClass().getResource("/gumtree.xml"))
    }
  }
}

object GumTreeConfiguration {

  private lazy val xml = XMLProvider.getXML()

  private lazy val crawlerXML = xml \ "crawler"

  private lazy val mailerXML = xml \ "mailer"

  private lazy val mapcenterXML = xml \ "mapcenter"

  private lazy val polygonsXML = xml \ "searcharea" \\ "polygon"

  private lazy val ignoredAdsXML = xml \ "ignoredads"

  lazy val polygons = PolygonsConfiguration(polygonsXML)

  lazy val mailer = MailerConfiguration(mailerXML)

  lazy val crawler = CrawlerConfiguration(crawlerXML)

  lazy val mapcenter = MapConfiguration(mapcenterXML)

  lazy val ignoredAds = IgnoredAdsConfiguration(ignoredAdsXML)

  lazy val resultPath = xml \ "resultpath" text

  lazy val refreshTime = (xml \ "refreshtime" text).toInt
}