package pl.gumtree.ads

import pl.gumtree.ads.types._

import scala.collection.mutable._
import scala.collection.JavaConversions._

import org.jsoup._
import org.jsoup.select._
import org.jsoup.nodes._

class GumTreeAd(url: GumTreeUrl) {

  private lazy val page = Jsoup.connect(url).get()

  def getApartment(): Apartment = {
    val adAttribiutes = getAttribiutes()
    val images = GumTreeImages(page)
    return Apartment(url, adAttribiutes, images)
  }

  private def getAttribiutes(): List[AdAttribiute] = {
    val table = getTableAttribiutes()
    val description = getDescription()
    return table :+ description
  }

  private def getTableAttribiutes(): List[AdAttribiute] = {
    val rows = page.select("#attributeTable tr").iterator.toList

    val adAttribiutes = new ListBuffer[AdAttribiute]()
    for (row <- rows) {
      val adAttribiute = convertToAttribiute(row)
      adAttribiutes += adAttribiute.get
    }

    return adAttribiutes.toList
  }

  private def convertToAttribiute(row: Element): Option[AdAttribiute] = {
    return row.select("td").iterator.toList match {
      case key :: value :: _ => Some((key.text, value.text))
      case _ => None
    }
  }

  private def getDescription(): AdAttribiute = {
    val description = page.select("#ad-desc").first
    return ("description", description.text)
  }

  private def getImages(): List[GumTreeImages] = {
    return null
  }
}