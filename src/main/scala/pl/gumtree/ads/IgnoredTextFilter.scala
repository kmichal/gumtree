package pl.gumtree.ads

import scala.collection.mutable.ListBuffer

class IgnoredTextFilter(ignoredTexts: List[String]) extends Filter {

  private val ignoredTextsInLowerCase = ignoredTexts.map(_.toLowerCase)

  def filter(apartments: List[Apartment]): List[Apartment] = {
    val filtredApartments = apartments.filterNot { apartment =>
      ignoredTextsInLowerCase.exists { ignoredText =>
        val descriptionInLowerCase = apartment.description.toLowerCase
        descriptionInLowerCase.contains(ignoredText)
      }
    }

    return filtredApartments
  }
}