package pl.gumtree.ads

import scala.language.postfixOps
import scala.collection.JavaConversions._

import com.vividsolutions.jts.geom._
import org.geotools.geometry.jts._

object GeometryFactory {

  val geometryBuilder = new GeometryBuilder()

  def point(coordinate: LatLon): Point = geometryBuilder.point(coordinate.lat, coordinate.lon)

  def point(lat: Double, lon: Double): Point = geometryBuilder.point(lat, lon)

  def polygon(coordinates: Double*): Polygon = geometryBuilder.polygon(coordinates : _*)

  def polygon(coordinates: List[LatLon]): Polygon = {
    val unpackedCoordinates = coordinates map { coordinate => List(coordinate.lat, coordinate.lon)} flatten
    
    return geometryBuilder.polygon(unpackedCoordinates : _*)
  }
}