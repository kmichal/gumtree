package pl.gumtree.ads

import pl.gumtree.ads.gui.AdsMap
import pl.gumtree.ads.gui.ApartmentMarkerModel
import pl.gumtree.ads.mail.GumTreeMailer

import java.io.File
import javax.swing.SwingUtilities
import java.awt.Color

import com.typesafe.scalalogging.slf4j.Logging

import com.vividsolutions.jts.geom._
import org.geotools.geometry.jts._

object SaveToFile {
  
  def printToFile(file: java.io.File)(block: java.io.PrintWriter => Unit) {
    val writer = new java.io.PrintWriter(file, "utf-8")
    try { block(writer) } finally { writer.close() }
  }
}

class GumTreeApp extends Logging {

  private val conf = GumTreeConfiguration

  private val polygonsFilter = getPolygonsFilter()

  private val ignoredTextFilter = getIgnoredTextFilter()

  private def getPolygonsFilter(): Filter = {
    val polygonsConf = conf.polygons
    val polygonsFilter = new PolygonsFilter(polygonsConf.polygons)
    return polygonsFilter
  }

  private def getIgnoredTextFilter(): Filter = {
    val ignoredAds = conf.ignoredAds
    val ignoredTextFilter = new IgnoredTextFilter(ignoredAds)
    return ignoredTextFilter
  }

  def getApartments(startPage: Int, endPage: Int): List[Apartment] = {
    val clawerConf = conf.crawler
    val crawler = new GumTreeCrawler(startPage, endPage, clawerConf.connection)
    val apartments = crawler.getApartments()    
    return apartments
  }

  def filter(apartments: List[Apartment]): List[Apartment] = {
    val apartmentsInArea = filterApartmentsByArea(apartments)
    val apartmentsInAreaWithoutIgnoredText = filterApartmentsByText(apartmentsInArea)
    return apartmentsInAreaWithoutIgnoredText
  }

  private def filterApartmentsByArea(apartments: List[Apartment]): List[Apartment] = {
    val apartmentsInArea = polygonsFilter.filter(apartments)
    return apartmentsInArea
  }

  private def filterApartmentsByText(apartments: List[Apartment]): List[Apartment] = {
    val adsWithoutIgnoredText = ignoredTextFilter.filter(apartments)
    return adsWithoutIgnoredText
  }

  def saveToFile(apartments: List[Apartment]) {
    SaveToFile.printToFile(new File(conf.resultPath)) { out =>
      out.println("<!doctype html><html><head><meta charset=\"utf-8\"></head><body>")
      apartments foreach out.println
      out.println("</body></html>")
    }
  }

  def openMap(): AdsMap = {
    val polygonsConf = conf.polygons
    val map = new AdsMap(polygonsConf.polygons)
    SwingUtilities.invokeLater(new Runnable() {
      override def run() {
        map.initGUI()
        map.setVisible(true)
      }
    })
    return map
  }

  def sendMail(apartments: List[Apartment]) {
    if (conf.mailer.password.isEmpty) {
      return
    }

    try {
      new GumTreeMailer().send(apartments)
    } catch {
      case e: Throwable => logger.debug(e.getMessage)
    }
  }
}

object GumTreeApp extends App with Logging {
  val clawerConf = GumTreeConfiguration.crawler
  logger.debug(s"startpage: ${ clawerConf.startPage } endpage: ${ clawerConf.endPage }")
  logger.debug(s"page: ${ clawerConf.connection }")

  val gumtree = new GumTreeApp()
  val apartments = gumtree.getApartments(clawerConf.startPage, clawerConf.endPage)

  logger.debug(s"before filter: ${ apartments.size } apartments")
  val filtredApartments = gumtree.filter(apartments)
  logger.debug(s"after filter: ${ filtredApartments.size } apartments")
  
  gumtree.saveToFile(filtredApartments.reverse)
  val map = gumtree.openMap()
  map.addApartments(filtredApartments map { apartment => ApartmentMarkerModel(apartment, Color.YELLOW) })
  //gumtree.sendMail(filtredApartments)

  val apartmentsCache = ApartmentsCache(apartments)
  while (true) {
    Thread.sleep(1000 * 60 * GumTreeConfiguration.refreshTime)

    val oldApartments = apartmentsCache.getAll
    val apartments = apartmentsCache(gumtree.getApartments(1, 2))
    val newApartments = apartments.filterNot { apartment => oldApartments.contains(apartment) }
    val newFiltredApartments = gumtree.filter(newApartments)

    logger.debug(s"new apartments after filter: ${ newFiltredApartments.size }")
    map.addApartments(newFiltredApartments map { apartment => ApartmentMarkerModel(apartment, Color.RED) })
    
    if (newFiltredApartments.nonEmpty) {
      gumtree.sendMail(newFiltredApartments)
    }

    val allApartmentsInArea = gumtree.filter(apartmentsCache.getAllSorted)
    gumtree.saveToFile(allApartmentsInArea)
  }
}