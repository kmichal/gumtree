package pl.gumtree.ads

import pl.gumtree.ads.types._

import scala.collection.mutable.ListBuffer
import scala.collection.JavaConversions._

import org.jsoup._
import org.jsoup.nodes._
import org.jsoup.select._

class GumTreeAdsPage(gumTreeUrlBuilder: GumTreeAdsUrlBuilder) {

  private type GumTreeAd = Element

  val gumtreeUrl = gumTreeUrlBuilder.build()

  def getPageUrls(): List[GumTreeUrl] = {
    val ads = selectAds()
    val urls = getUrls(ads)
    return urls
  }

  private def selectAds(): List[GumTreeAd] = {
    val gumtreeDocument = Jsoup.connect(gumtreeUrl).timeout(10000).get()
    val ads = gumtreeDocument.select(".rrow")

    return ads.iterator.toList
  }

  private def getUrls(ads: List[GumTreeAd]): List[GumTreeUrl] = {
    val urls = new ListBuffer[GumTreeUrl]()

    for (ad <- ads) {
      val url = ad.select(".ar-title a").attr("href")
      val urlIsValid = !url.isEmpty
     
      if (urlIsValid) {
        urls += url
      }
    }

    return urls.toList
  }
}
