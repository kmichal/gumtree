package pl.gumtree.ads

import pl.gumtree.ads.types._
import pl.gumtree.ads.geocoder.AddressGeocoderCache

import java.util.Date
import java.text.SimpleDateFormat

import scala.collection.mutable._

import com.google.common.base._

object Apartment {
  
  private val withTimeFormater = new SimpleDateFormat("yyyy-MM-dd HH:mm")

  private val withoutTimeFormater = new SimpleDateFormat("yyyy-MM-dd")

  private val dateParser = new SimpleDateFormat("dd/MM/yyyy")

  def apply(url: GumTreeUrl, adAttribiutes: List[AdAttribiute], images: List[GumTreeImage]): Apartment = {
    val adAttribiutesMap = adAttribiutes.toMap

    val addDate = parseDate(adAttribiutesMap.get("Data dodania"))
    val price = parsePrice(adAttribiutesMap.get("Cena"))
    val address = cleanUpAddress(adAttribiutesMap.get("Adres"))
    val description = adAttribiutesMap("description")
    
    return Apartment(url, addDate, price, address, description, adAttribiutes, images)
  }

  private def parsePrice(rawPrice: Option[String]): Option[Int] = {
    return rawPrice match {
      case Some(price) => {
        val rawPrice = CharMatcher.JAVA_DIGIT.retainFrom(price)
        if (!rawPrice.isEmpty) Some(rawPrice.toInt / 100) else None
      }
      case None => None
    }
  }

  private def parseDate(rawDate: Option[String]): Option[Date] = {
    return rawDate match {
      case Some(date) => Some(dateParser.parse(date))
      case None => None
    }
  }

  private def cleanUpAddress(rawAddress: Option[String]): Option[String] = {
    return rawAddress match {
      case Some(address) => Some(cleanUpAddress(address))
      case None => None
    }
  }

  private def cleanUpAddress(rawAddress: String): String = {
    return rawAddress.replace("Pokaż mapę", "").trim
  }
}

case class Apartment(url: GumTreeUrl, addDate: Option[Date], price: Option[Int], address: Option[String], description: String, others: List[AdAttribiute], images: List[GumTreeImage]) {

  val timestamp = new Date()

  lazy val latLon = address match {
    case Some(address) => AddressGeocoderCache(address)
    case None => None
  }

  override def toString(): String = {
    val link = address match {
      case Some(address) => address
      case None => url
    }

    val formatedDate = addDate match {
      case Some(date) => Apartment.withoutTimeFormater.format(date)
      case None => ""
    }

    val formatedTimestamp = Apartment.withTimeFormater.format(timestamp)

    val html =
    <div>
      <a href={ url }>{ formatedDate }, { link }</a><br />
      Cena: { price.orNull }<br />
      Timestamp: { formatedTimestamp }<br />
      { description }<br />
      { for (image <- images) yield <img src={ image.src } /><br /> }
    </div>

    return html.toString
  }
}