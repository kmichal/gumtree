package pl.gumtree.ads.geocoder

import pl.gumtree.ads.LatLon

trait AddressGeocoder {
  
  def find(address: String): Option[LatLon]
}