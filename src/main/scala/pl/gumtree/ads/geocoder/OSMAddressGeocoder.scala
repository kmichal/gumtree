package pl.gumtree.ads.geocoder

import pl.gumtree.ads.LatLon

import scala.collection.JavaConversions._

import com.typesafe.scalalogging.slf4j.Logging
import fr.dudie.nominatim.client.JsonNominatimClient
import fr.dudie.nominatim.model.Address
import org.apache.http.impl.client.HttpClientBuilder

class OSMAddressGeocoder extends AddressGeocoder with Logging {

  private val geocoder = createGeocoder()

  private def createGeocoder(): JsonNominatimClient = {
    val httpClient = HttpClientBuilder.create.build
    return new JsonNominatimClient(httpClient, "fakeemail@gmail.com")
  }

  override def find(address: String): Option[LatLon] = {
    return geocoder.search(address).headOption match {
      case Some(result) => convertToLatLon(result)
      case None => {
        logger.debug(s"No results from OSM Geocoder for address: $address")
        None
      }
    }
  }

  private def convertToLatLon(result: Address): Option[LatLon] = {
    if (!isAccurate(result)) {
      return None
    }

    val latLon = LatLon(result.getLatitude, result.getLongitude)
    return Some(latLon)
  }

  private def isAccurate(result: Address): Boolean = {
    val isAtLeastRoad = result.getAddressElements.map{ e => (e.getKey, e.getValue) }.toMap.containsKey("road")
    return isAtLeastRoad
  }
}