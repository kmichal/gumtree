package pl.gumtree.ads.geocoder

import pl.gumtree.ads.LatLon

import scala.collection.mutable.HashMap

object AddressGeocoderCache {
  
  private val cache = new HashMap[String, Option[LatLon]]

  private val geocoder: AddressGeocoder = new GoogleAddressGeocoder()

  def apply(address: String): Option[LatLon] = {
    return cache.getOrElseUpdate(address, geocoder.find(address))
  }
}