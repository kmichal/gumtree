package pl.gumtree.ads.geocoder

import pl.gumtree.ads.LatLon

import scala.collection.JavaConversions._

import com.typesafe.scalalogging.slf4j.Logging

import com.google.code.geocoder._
import com.google.code.geocoder.model._

class GoogleAddressGeocoder extends AddressGeocoder with Logging {

  private val geocoder = createGeocoder()

  private def createGeocoder(): Geocoder = {
    return new Geocoder()
    // val advancedGeocoder = new AdvancedGeoCoder()
    // advancedGeocoder.getHttpClient().getHostConfiguration().setProxy("209.239.114.86", 8080)
    // return advancedGeocoder
  }

  override def find(address: String): Option[LatLon] = {
    Thread.sleep(300) // because of AddressGeocoder limitation
    val geocoderRequest = new GeocoderRequestBuilder().setAddress(address).setLanguage("pl").getGeocoderRequest()
    val geocoderResponse = geocoder.geocode(geocoderRequest)

    return geocoderResponse.getResults().headOption match {
      case Some(result) => convertToLatLon(result)
      case None => {
        logger.debug(s"No results from Google Geocoder for address: $address")
        None
      }
    }
  }

  private def convertToLatLon(result: GeocoderResult): Option[LatLon] = {
    if (!isAccurate(result)) {
      return None
    }

    val latLng = result.getGeometry.getLocation
    val latLon = LatLon(latLng.getLat.doubleValue, latLng.getLng.doubleValue)
    return Some(latLon)
  }

  private def isAccurate(result: GeocoderResult): Boolean = {
    return result.getTypes.contains("street_address") || result.getTypes.contains("route")
  }
}