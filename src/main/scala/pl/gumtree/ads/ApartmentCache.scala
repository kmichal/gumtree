package pl.gumtree.ads

import scala.collection.mutable.LinkedHashMap
import scala.language.implicitConversions

class ApartmentsCache {

  private val cache = new LinkedHashMap[String, Apartment]

  def getOrElseUpdate(apartment: Apartment): Apartment = {
    return cache.getOrElseUpdate(apartment.url, apartment)
  }

  def getOrElseUpdate(apartments: List[Apartment]): List[Apartment] = {
    return apartments.map { apartment => getOrElseUpdate(apartment) }
  }

  def getAll(): List[Apartment] = {
    return cache.values.toList
  }

  def getAllSorted(): List[Apartment] = {
    return cache.values.toList.sortBy { apartment => apartment.timestamp }.reverse
  }

  def apply(apartments: List[Apartment]): List[Apartment] = getOrElseUpdate(apartments)

  def apply(apartment: Apartment): Apartment = getOrElseUpdate(apartment)
}

object ApartmentsCache {

  def apply(apartments: List[Apartment]): ApartmentsCache = {
    val cache = new ApartmentsCache()
    cache(apartments)
    return cache
  }

  implicit def getAllFromCache(cache: ApartmentsCache): List[Apartment] = cache.getAll
}