package pl.gumtree.ads

import pl.gumtree.ads.types._

import scala.collection.mutable._
import scala.collection.JavaConversions._

import org.jsoup._
import org.jsoup.select._
import org.jsoup.nodes._

case class GumTreeImage(src: Url)

object GumTreeImages {
  def apply(page: Document): List[GumTreeImage] = new GumTreeImages(page).getImages
}

class GumTreeImages(page: Document) {

  def getImages(): List[GumTreeImage] = {
    val rawImages = page.select(".imageNavs img").iterator.toList
    
    val images = new ListBuffer[GumTreeImage]()
    for (rawImage <- rawImages) {
      val image = getImage(rawImage)
      images += image
    }

    return images.toList
  }

  private def getImage(rawImage: Element): GumTreeImage = {
    val smallImageSrc = rawImage.attr("src")
    val bigImageSrc = convertToBigImageSrc(smallImageSrc)
    val image = GumTreeImage(bigImageSrc)
    return image
  } 

  private def convertToBigImageSrc(smallImageSrc: Url): Url = {
    val bigImageSrc = smallImageSrc.dropRight(6) + "20.JPG"
    return bigImageSrc
  }
}