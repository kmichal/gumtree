name := "gumtree"

version := "1.0"

scalaVersion := "2.10.3"

fork := true

scalacOptions ++= Seq("-unchecked", "-deprecation", "-feature")

seq(com.github.retronym.SbtOneJar.oneJarSettings: _*)

org.scalastyle.sbt.ScalastylePlugin.Settings

resolvers += "Open Source Geospatial Foundation Repository" at "http://download.osgeo.org/webdav/geotools/"

// UTILS

libraryDependencies += "com.google.guava" % "guava" % "14.0-rc1"

libraryDependencies += "com.google.code.findbugs" % "jsr305" % "2.0.1"

// JSOUP

libraryDependencies += "org.jsoup" % "jsoup" % "1.7.2"

// LOG

libraryDependencies += "org.slf4j" % "slf4j-api" % "1.7.3"

libraryDependencies += "ch.qos.logback" % "logback-classic" % "1.0.10"

libraryDependencies += "com.typesafe" %% "scalalogging-slf4j" % "1.0.1"

// GEO

libraryDependencies += "com.google.code.geocoder-java" % "geocoder-java" % "0.15"

libraryDependencies += "fr.dudie" % "nominatim-api" % "2.0.2"

libraryDependencies += "org.apache.httpcomponents" % "httpclient" % "4.3"

libraryDependencies += "org.geotools" % "gt-api" % "9.4"

libraryDependencies += "org.geotools" % "gt-main" % "9.4"

// MAIL

libraryDependencies += "org.codemonkey.simplejavamail" % "simple-java-mail" % "2.1"

// TEST

libraryDependencies += "org.scalatest" %% "scalatest" % "2.1.0" % "test"

libraryDependencies += "org.mockito" % "mockito-all" % "1.9.5" % "test"